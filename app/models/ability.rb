class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # guest user (not logged in)

    # can :manage, :all if user.admin?

    can :read, :all      # permissions for every user, even if not logged in    
    if user.present?     # additional permissions for logged in users (they can manage their posts)
      #  colocar permissões
    end

  end
  def to_list
    rules.map do |rule|
      object = { action: rule.actions, subjectName: rule.subjects.map{ |s| s.is_a?(Symbol) ? s : s.name } }
      object[:conditions] = rule.conditions unless rule.conditions.blank?
      object[:inverted] = true unless rule.base_behavior
      object
    end
  end
end